package com.example.cardpresentation

import android.graphics.drawable.Icon
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.cardpresentation.ui.theme.CardPresentationTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CardPresentationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CardPresentationApp()
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Composable
fun CardPresentationApp() {
    Column(modifier = Modifier
        .background(Color(0xFFd2e8d4))
    ) {
        UserCardInfo(
            name = "Jean Marco",
            position = "Android Developer Extraordinary",
            modifier = Modifier
                .weight(3f)
        )
        UserCardContactInfo(modifier = Modifier
            .weight(1f)
        )
    }
}


@Composable
private fun UserCardInfo(name: String, position: String, modifier: Modifier = Modifier) {
    Column(
        modifier = modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        UserCardImage(modifier = Modifier
            .height(120.dp)
            .width(120.dp))

        Text(
            text = name,
            fontSize = 45.sp,
            modifier = Modifier.padding(vertical = 4.dp),
            fontWeight = FontWeight.Light
        )
        Text(
            text = position,
            color = Color(0xFF026e3c),
            fontWeight = FontWeight.Bold
        )
    }
}

@Composable
private fun UserCardImage(modifier: Modifier = Modifier) {
    Box(modifier = modifier.background(Color(0xFF073042))) {
        val userImage = painterResource(id = R.drawable.android_logo)

        Image(painter = userImage, contentDescription = null)
    }
}


@Composable
private fun UserCardContactInfo(modifier: Modifier = Modifier) {
    Box(
        modifier = modifier
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column {
            ContactInfoItem(icon = Icons.Filled.Phone, value = "+11(123) 444 555 666")
            ContactInfoItem(icon = Icons.Filled.Share, value = "@AndroidDev" )
            ContactInfoItem(icon = Icons.Filled.Email, value = "jean.miranda@gmail.com")
        }
    }
}

@Composable
private fun ContactInfoItem(
    icon: ImageVector,
    value: String,
    modifier: Modifier = Modifier)
{
    Row(modifier = modifier.padding(bottom = 15.dp)) {
        Icon(icon, contentDescription = null, tint = Color(0xFF026e3c))
        Spacer(modifier = Modifier.width(20.dp))
        Text(text = value)
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    CardPresentationTheme {
        CardPresentationApp()
    }
}